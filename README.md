# Circular dependencies problem in node

> don't use `module.exports = ...`



A circular dependency of files `a.js -> b.js -> c.js -> a.js`

The `a.js` file uses `module.exports` and as you can see, the final object is empty, it should have a property of `one: 1` inside it

```
➜  circular-deps (master) ✔ node a.js
{}
```

Now we use the `exports.default` with the `a1.js` file and you can see the result is `{ one: 1 }` as we expect it to be
```
➜  circular-deps (master) ✔ node a1.js
{ one: 1 }
```
